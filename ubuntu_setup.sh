#
# This is no functional bash script atm. It's more of copypasta dish.

sudo apt -y update &&
    sudo apt install -y software-properties-common python3-pip &&
    pip3 install -U \
        catkin_tools \
        psutil \
        nvidia-ml-py

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list' &&
    sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 &&
    sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/gazebo-stable.list' &&
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D2486D2DD83DB69272AFE98867170598AF249743

sudo apt update -y &&
    sudo apt install -y \
        ros-melodic-desktop \
        ros-melodic-robot-localization \
        python-rosdep \
        python-rosinstall \
        python-rosinstall-generator \
        python-wstool \
        build-essential

rosdep init &&
    rosdep update &&
    apt upgrade


echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc && bash -c "source ~/.bashrc"

rosdep install -y --from-paths src --ignore-src --rosdistro melodic

source deve/setup.bash