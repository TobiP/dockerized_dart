FROM ubuntu:18.04

# TODO: update README.MD -> Use Ubuntu 18.04 to avoid missing package libboost-system1.65.1

# RUN apt -y update && apt install -y lsb-core wget \
#     && sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -sc` main" > /etc/apt/sources.list.d/ros-latest.list' \
#     && wget http://packages.ros.org/ros.key -O - | apt-key add -

# TODO: update README.MD -> pip python3-pip + pip3

ENV UBUNTU_VERSION=bionic
ARG DEBIAN_FRONTEND=noninteractive

RUN apt -y update && apt install -y software-properties-common python3-pip \
    && pip3 install -U \
    catkin_tools \
    psutil \
    nvidia-ml-py 

# TODO: update README.MD -> packages not available anymore; add this
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $UBUNTU_VERSION main" > /etc/apt/sources.list.d/ros-latest.list' \
    && apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654 \
    && sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/gazebo-stable.list' \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D2486D2DD83DB69272AFE98867170598AF249743

RUN apt update  -y \
    && apt install -y  \
    ros-melodic-desktop-full \
    ros-melodic-robot-localization \
    python-rosdep \
    python-rosinstall \
    python-rosinstall-generator \
    python-wstool \
    build-essential

RUN rosdep init \
    && rosdep update -y \
    && apt upgrade -y

# Maybe we need this?
RUN sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/gazebo-stable.list'
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D2486D2DD83DB69272AFE98867170598AF249743
RUN apt update
RUN apt upgrade

# Add ROS environment variables
RUN echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc \ 
    && bash -c "source ~/.bashrc"
# FIXME: This does not work as expected. Maybe "~" is the culprit?

# !!!! The following command needs the dart fsd repository with all its initialized submodules!
COPY fsd /fsd
WORKDIR /fsd
RUN rosdep install -y --from-paths src --ignore-src --rosdistro melodic
# Cleanup so we can mount this again. And hopefully free some space
RUN rm -rf /fsd

RUN apt update \
    && apt install -y \
    mesa-utils \
    libgl1-mesa-dri

# Now we can start to work
ENTRYPOINT bash

## This fails probably because XQuartz on mac has no real 3D support (libgdx) 
## Command for using xquartz 
#  - Start XQuartz, then:
#  xhost + ${hostname} 
## docker run -e DISPLAY=host.docker.internal:0 -v -it --name my-ros ros:experiment